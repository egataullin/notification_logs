class ShopDeferred
  attr_reader :shop

  delegate :today_schedule,
           :active?,
           :scheduled_today?,
           to: :shop

  delegate :call_center_work_time_start,
           :call_center_work_time_end,
           to: :today_schedule

  def self.process(shop:)
    new(shop).process
  end

  def initialize(shop)
    @shop = shop
  end

  def process
    return unless active? && scheduled_today?
    log_start_time
    log_end_time
  end

  private

  def log_start_time
    if today_work_time_start_difference > 0
      log_today_start_active_state(today_work_time_start_difference + 1.minute)
    else
      log_today_start_active_state(1.seconds)
    end
  end

  def log_end_time
    if today_work_time_end_difference > 0
      log_today_end_active_state(today_work_time_end_difference + 1.minute)
    else
      log_today_end_active_state(1.seconds)
    end
  end

  def log_today_end_active_state(time_difference)
    CheckProcessingWorker.perform_in(time_difference,
                                    'Check::ShopInactive', {shop_id: shop.id,
                                                            schedule_id: today_schedule.id,
                                                            today_schedule_updated_at: today_schedule.updated_at})
  end

  def log_today_start_active_state(time_difference)
    CheckProcessingWorker.perform_in(time_difference,
                                    'Check::ShopActivated', {shop_id: shop.id,
                                                             schedule_id: today_schedule.id,
                                                             today_schedule_updated_at: today_schedule.updated_at})
  end

  def to_today_time(time)
    Time.zone.parse(time.to_s(:time))
  end

  def today_work_time_end_difference
    to_today_time(call_center_work_time_end) - Time.zone.now
  end

  def today_work_time_start_difference
    to_today_time(call_center_work_time_start) - Time.zone.now
  end

  def tomorow_work_time_start_difference
    to_today_time(call_center_work_time_start) + 1.day - Time.zone.now
  end

end
