class PushStrategy
  LOGGER_HOST = 'some_host'
  attr_reader :log_template, :error, :response

  def initialize(log_template)
    @log_template = log_template
  end

  def process
    make_request
  end

  def request_body
    log_template.request_load.to_json
  end

  private

  def make_request
    rescue_exeptions do
      @response = connection.post do |req|
        req.headers['Content-Type'] = 'application/json'
        req.body = request_body
      end
    end
  end

  def rescue_exeptions
    @error = nil
    yield
    success = response.success?
    unless success
      @error = response.status
    end
    success
    rescue Faraday::Error::ClientError => e
      @error = e.message
      false
  end

  def connection
    @connection ||= begin
      Faraday.new(url: LOGGER_HOST) do |faraday|
        faraday.request  :url_encoded
        faraday.adapter  Faraday.default_adapter
      end
    end
  end

end
