# вызов отложенных проверок,
# с опциональной посылкой нотификации
ShopDeferred.process(shop: shop)

# безусловная асинхронная посылка нотификации
order.add_after_commit_command do |order_after|
  BusinessLogWorker.perform_async('Templates::OrderChangeStatus',
                                  Time.zone.now,
                                  {order_id: order_after.id,
                                   status_before: 'status_before',
                                   status_after: order_after.status})
end

# add_after_commit_command
module AfterCommitCommands
  extend ActiveSupport::Concern
  included do
    after_commit :__call_after_commit_commands
  end

  def after_commit_commands
    @after_commit_commands ||= []
  end

  def clear_after_commit_commands
    @after_commit_commands = []
  end

  def add_after_commit_command(&block)
    raise(LocalJumpError, 'no block given (yield)') unless block_given?
    __add_after_commit_command block
  end

  private

  def __add_after_commit_command(proc)
    after_commit_commands << proc
  end

  def __call_after_commit_commands
    after_commit_commands.delete_if { |c| c.call(self); true }
  end

end

