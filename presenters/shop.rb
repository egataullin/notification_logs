require 'roar/decorator'
require 'roar/json'
module Presenters
  class Shop < Roar::Decorator
    include Roar::JSON
    property :id, render_nil: true
    #other shop fields
  end
end

