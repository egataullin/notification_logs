require 'roar/json'
require 'roar/decorator'
module Presenters
  class Order < Roar::Decorator
    include Roar::JSON

    property :id, render_nil: true
    #other order fields
    collection :goods, decorator: Presenters::Good, as: :goods

  end
end
