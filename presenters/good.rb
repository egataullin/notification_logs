require 'roar/decorator'
require 'roar/json'
module Presenters
  class Good < Roar::Decorator
    include Roar::JSON
    property :id, render_nil: true
    # other good fields
  end
end

