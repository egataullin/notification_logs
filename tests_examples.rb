# примеры некоторых тестов системы

RSpec.shared_examples "common templates spec" do

  context "initialized with timestamp" do
    it "has valid timestamp" do
      expect(template.timestamp).to eq(timestamp)
    end
  end

  describe "#request_load" do
    it "returns valid hash" do
      expect(template.request_load).to match(a_hash_including(_type: described_class.name.split('::')[-1].underscore,
                                                              _timestamp: timestamp))
    end

  end
end

RSpec.shared_examples "base template spec" do
  let(:timestamp) { Time.zone.now }
  let(:template) { described_class.new(timestamp: timestamp) }

  it_behaves_like "common templates spec"

end

RSpec.describe Templates::Base do
  it_behaves_like "base template spec"
end


##################################

RSpec.shared_examples "order base templates spec" do

  it_behaves_like "common templates spec"

  context "initialized with order" do
    it "has valid courier_company" do
      expect(template.order).to eq(order)
    end
  end
  describe "#request_load" do

    it "returns valid special hash" do
      expect(template.request_load).to match(a_hash_including(order: Presenters::Order.represent(order).as_json))
    end
  end
end

##################################


RSpec.shared_examples "order change status template" do
  let(:timestamp) { Time.zone.now }
  let(:status_before) { 'published' }
  let(:status_after) { 'confirmed' }
  let(:order) { FactoryGirl.create(:order, :with_goods, :with_additional_data) }
  let(:template) { described_class.new(timestamp: timestamp,
                                       status_before: status_before,
                                       status_after: status_after,
                                       order_id: order.id) }

  it_behaves_like "order base templates spec"

  describe "#request_load" do

    it "returns valid max period" do
      expect(template.request_load).to match(a_hash_including(status_before: status_before,
                                                              status_after: status_after))
    end
  end
end

RSpec.describe Templates::OrderChangeStatus do
  it_behaves_like "order change status template"
end
