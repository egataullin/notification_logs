class BusinessLogWorker
  include Sidekiq::Worker

  def perform(log_class, timestamp, params = {})
    strategy = Strategy.new(
      log_class.constantize.new({timestamp: timestamp}.merge(params.symbolize_keys)))
    unless strategy.process
      Rails.logger.info "Could not send BusinessLog #{strategy.error}"
    end
  end
end
