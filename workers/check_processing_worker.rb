class CheckProcessingWorker
  include Sidekiq::Worker

  def perform(check_class, params = {})
    check_class.constantize.process(params.symbolize_keys)
  end
end
