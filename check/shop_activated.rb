class Check::ShopActivated < Check::ShopActiveBase
  delegate :call_center_work_time_start,
           :call_center_work_time_end,
            to: :schedule

  def check_and_log
    if (to_today_time(call_center_work_time_end) > Time.zone.now) &&
       (to_today_time(call_center_work_time_start) < Time.zone.now)
      log_not_active
    end
  end

  def log_not_active
    log(Templates::ShopActivated, Time.zone.now, {shop: shop})
  end

end
