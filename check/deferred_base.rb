class Check::DeferredBase
  attr_reader :log_strategy
  private

  def log(log_class, timestamp, params = {})
    @log_strategy = PushStrategy.new(
      log_class.new({timestamp: timestamp}.merge(params.symbolize_keys)))
    unless @log_strategy.process
      Rails.logger.info "Could not send BusinessLog #{@log_strategy.error}"
    end
  end
end
