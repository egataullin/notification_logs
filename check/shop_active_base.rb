class Check::ShopActiveBase < Check::DeferredBase
  attr_reader :shop, :today_schedule_updated_at, :schedule
  delegate :active?,
           to: :shop

  def self.process(shop_id:, today_schedule_updated_at:, schedule_id:)
    shop = Shop.find(shop_id)
    new(shop,
        Time.zone.parse(today_schedule_updated_at),
        shop.shop_day_schedules.find(schedule_id)).process
  end

  def initialize(shop, today_schedule_updated_at, schedule)
    @shop = shop
    @today_schedule_updated_at = today_schedule_updated_at
    @schedule = schedule
  end

  def process
    return unless active? && current_update
    check_and_log
  end

  private

  def current_update
    ((schedule.updated_at - 1.seconds) < today_schedule_updated_at) &&
    ((schedule.updated_at + 1.seconds) > today_schedule_updated_at)
  end

  # rewrite at subclass
  def check_and_log
  end

  def to_today_time(time)
    Time.zone.parse(time.to_s(:time))
  end

end
