class Check::ShopInactive < Check::ShopActiveBase

  delegate :call_center_work_time_end,
           to: :schedule

  private

  def check_and_log
    if (to_today_time(call_center_work_time_end) < Time.zone.now)
      log_not_active
    end
  end

  def log_not_active
    log(Templates::ShopInactive, Time.zone.now, {shop: shop})
  end

end
