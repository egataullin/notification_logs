class Templates::OrderChangeStatus < Templates::Base

  attr_reader :order, :status_before, :status_after

  def initialize(timestamp:, order_id:, status_before:, status_after:)
    @order = Order.find(order_id)
    @status_before = status_before
    @status_after = status_after
    @timestamp = timestamp
  end

  private

  def special_load
    {order: order_hash,
     status_before: status_before,
     status_after: status_after}
  end

  private

  def order_hash
    Presenters::Order.represent(order).as_json
  end

end
