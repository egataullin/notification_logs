class Templates::ShopInactive < Templates::Base

  attr_reader :shop

  def initialize(timestamp:, shop:)
    @shop = shop
    @timestamp = timestamp
  end

  private

  def special_load
    {shop: shop_hash}
  end

  def shop_hash
    Presenters::Shop.represent(shop).as_json
  end

end
