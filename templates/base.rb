class Templates::Base

  attr_reader :timestamp

  def initialize(timestamp:)
    @timestamp = timestamp
  end

  def request_load
    base_load.merge(special_load)
  end

  private

  def base_load
    {_type: log_type,
     _timestamp: timestamp}
  end

  # rewrite this method at subclass
  # returns Hash
  def special_load
    {}
  end

  def log_type
    self.class.name.split('::')[-1].underscore
  end

end
